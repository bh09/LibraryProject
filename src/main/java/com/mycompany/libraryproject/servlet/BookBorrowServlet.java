package com.mycompany.libraryproject.servlet;

import com.mycompany.libraryproject.dto.BookStatusDTO;
import com.mycompany.libraryproject.service.BookBorrowService;
import com.mycompany.libraryproject.service.BookService;
import com.mycompany.libraryproject.service.UserService;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@ServletSecurity(@HttpConstraint(rolesAllowed = {"admin", "user", "librarian"}))
@WebServlet(name = "BookBorrowServlet", urlPatterns = {"/BookBorrowServlet"})
public class BookBorrowServlet extends HttpServlet {

    @Inject
    UserService userService;

    @Inject
    BookService bookService;

    @Inject
    BookBorrowService bookBorrowService;

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        request.setAttribute("userList", userService.getAll());
        request.setAttribute("bookList", bookService.getAll());
        request.getRequestDispatcher("WEB-INF/bookBorrow.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        BookStatusDTO dto = new BookStatusDTO();
        dto.setBookId(Long.parseLong(request.getParameter("book")));
        dto.setUserId(Long.parseLong(request.getParameter("user")));

        bookBorrowService.save(dto);

        response.sendRedirect(this.getServletName());
    }

}
