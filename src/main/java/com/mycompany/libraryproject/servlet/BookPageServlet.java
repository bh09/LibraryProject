
package com.mycompany.libraryproject.servlet;

import com.mycompany.libraryproject.dto.BookDTO;
import com.mycompany.libraryproject.service.BookService;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(name = "BookPageServlet", urlPatterns = {"/BookPageServlet"})
public class BookPageServlet extends HttpServlet {

    @Inject
    BookService bookService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Long id = Long.parseLong(request.getParameter("id"));

        BookDTO book = bookService.get(id);

        request.setAttribute("book", book);

        request.getRequestDispatcher("WEB-INF/bookPage.jsp").forward(request, response);

    }

}
