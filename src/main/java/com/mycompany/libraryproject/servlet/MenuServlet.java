package com.mycompany.libraryproject.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wolfp
 */
@WebServlet(name = "MenuServlet", urlPatterns = {"/MenuServlet"})
public class MenuServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        Boolean isLoggedIn = null != request.getRemoteUser();

        request.setAttribute("isLoggedIn", isLoggedIn);
        request.getRequestDispatcher("WEB-INF/include/menu.jsp").include(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        Boolean isLoggedIn = null != request.getRemoteUser();

        request.setAttribute("isLoggedIn", isLoggedIn);
        request.getRequestDispatcher("WEB-INF/include/menu.jsp").include(request, response);
    }
}
