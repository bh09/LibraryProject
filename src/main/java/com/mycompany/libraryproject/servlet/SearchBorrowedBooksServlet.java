package com.mycompany.libraryproject.servlet;

import com.mycompany.libraryproject.dto.BookStatusDTO;
import com.mycompany.libraryproject.service.BookBorrowService;
import com.mycompany.libraryproject.service.UserService;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Fanni
 */
@ServletSecurity(@HttpConstraint(rolesAllowed = {"admin", "user", "librarian"}))
@WebServlet(name = "SearchBorrowedBooksServlet", urlPatterns = {"/SearchBorrowedBooksServlet"})
public class SearchBorrowedBooksServlet extends HttpServlet {

    @Inject
    UserService userService;

    @Inject
    BookBorrowService bookBorrowService;

    BookStatusDTO dto = new BookStatusDTO();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        String userRole = request.isUserInRole("admin") ? "admin" : "user";
        String user = request.getRemoteUser();

        System.out.println("!!! User: " + user + ", role: " + userRole);

        if (request.getParameterMap().containsKey("action") && request.getParameterMap().containsKey("id")) {
            String action = request.getParameter("action");
            BookStatusDTO bsdto = new BookStatusDTO();
            bsdto.setId(Long.parseLong(request.getParameter("id")));

            switch (action) {
                case "giveBack":
                    bookBorrowService.giveBack(bsdto);
                    break;
                case "extension":
                    bookBorrowService.borrowExtension(bsdto);
                    break;
            }
            response.sendRedirect(this.getServletName());
        } else {
            request.setAttribute("userRole", userRole);

            if ("admin".equals(userRole)) {
                request.setAttribute("userList", userService.getAll());
            } else if ("user".equals(userRole)) {
                request.setAttribute("resultList", bookBorrowService.searchBorrowedBook(dto, user));
            }

            request.getRequestDispatcher("WEB-INF/searchBorrowedBooks.jsp").forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        dto.setUserId(Long.parseLong(request.getParameter("user")));
        request.setAttribute("userList", userService.getAll());
        request.setAttribute("resultList", bookBorrowService.searchBorrowedBook(dto, "admin"));

        request.getRequestDispatcher("WEB-INF/searchBorrowedBooks.jsp").forward(request, response);
    }

}
