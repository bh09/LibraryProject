/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.libraryproject.servlet;

import com.mycompany.libraryproject.dto.RoleDTO;
import com.mycompany.libraryproject.dto.UserDTO;
import com.mycompany.libraryproject.service.RoleService;
import com.mycompany.libraryproject.service.UserService;
import java.io.IOException;
import java.io.PrintWriter;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Fanni
 */
@ServletSecurity(@HttpConstraint(rolesAllowed = {"admin", "librarian"}))
@WebServlet(name = "ChangeUserRoleServlet", urlPatterns = {"/ChangeUserRoleServlet"})
public class ChangeUserRoleServlet extends HttpServlet {
    
    @Inject
    UserService userService;
    
    @Inject
    RoleService roleService;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        request.setAttribute("userList", userService.getAll());
        request.setAttribute("roleList", roleService.getAll());
        request.getRequestDispatcher("WEB-INF/changeRole.jsp").forward(request, response);
        
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        UserDTO userDdto = new UserDTO();
        RoleDTO roleDto = new RoleDTO();
        userDdto.setId(Long.parseLong(request.getParameter("user")));
        roleDto.setId(Long.parseLong(request.getParameter("role")));
        
        userService.changeRole(userDdto, roleDto);
        
        response.sendRedirect(this.getServletName());
    }
    
}
