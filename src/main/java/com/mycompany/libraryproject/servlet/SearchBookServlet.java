package com.mycompany.libraryproject.servlet;

import com.mycompany.libraryproject.dto.BookDTO;
import com.mycompany.libraryproject.service.BookService;
import com.mycompany.libraryproject.service.SearchBookService;
import java.io.IOException;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wolfp
 */
@WebServlet(name = "SearchBookServlet", urlPatterns = {"/SearchBookServlet"})
public class SearchBookServlet extends HttpServlet {

    @Inject
    SearchBookService searchBookService;

    @Inject
    BookService bookService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        String userRole = request.isUserInRole("admin") ? "admin" : "user";

        if (request.getParameterMap().containsKey("action") && "delete".equals(request.getParameter("action"))) {
            BookDTO bdto = new BookDTO();
            bdto.setId(Long.parseLong(request.getParameter("id")));
            bookService.delete(bdto);
            response.sendRedirect(getServletName());
        } else {
            request.setAttribute("role", userRole);
            request.getRequestDispatcher("WEB-INF/searchBooks.jsp").forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        String userRole = request.isUserInRole("admin") ? "admin" : "user";
        List<BookDTO> resultList = searchBookService.searchBook(request.getParameter("writer"), request.getParameter("bookTitle"), request.getParameter("bookCategory"));
        request.setAttribute("resultList", resultList);
        request.setAttribute("role", userRole);

        request.getRequestDispatcher("WEB-INF/searchBooks.jsp").forward(request, response);
    }
}
