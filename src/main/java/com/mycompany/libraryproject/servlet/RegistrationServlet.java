package com.mycompany.libraryproject.servlet;

import com.mycompany.libraryproject.dto.UserDTO;
import com.mycompany.libraryproject.service.UserService;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.codec.digest.DigestUtils;

@WebServlet(name = "Registration", urlPatterns = {"/Registration"})

public class RegistrationServlet extends HttpServlet {

    @Inject
    UserService userService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        request.setAttribute("userList", userService.getAll());
        request.setAttribute("registered", getServletContext().getAttribute("registered"));

        if (null != getServletContext().getAttribute("alertMsg")) {
            request.setAttribute("alertMsg", getServletContext().getAttribute("alertMsg"));
        }
       request.getRequestDispatcher("WEB-INF/registration.jsp").forward(request, response);
       getServletContext().setAttribute("alertMsg", null);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        UserDTO userDTO = new UserDTO();

        String firstName = request.getParameter("firstName");
	String lastName = request.getParameter("lastName");
	String password = request.getParameter("password");
	String email = request.getParameter("email");

        password = DigestUtils.md5Hex(password);

        userDTO.setFirstName(firstName);
        userDTO.setLastName(lastName);
        userDTO.setEmail(email);
        userDTO.setPassword(password);

        if(!userService.save(userDTO)) {
            getServletContext().setAttribute("alertMsg", "Felhasználó már létezik!");
            RequestDispatcher rd=request.getRequestDispatcher("WEB-INF/registration.jsp");
            rd.include(request, response);
            System.out.println("Felhasználó már létezik!");
        } else {
            getServletContext().setAttribute("alertMsg", "Sikeres regisztráció!");
            RequestDispatcher rd=request.getRequestDispatcher("WEB-INF/registration.jsp");
            rd.include(request, response);
            System.out.println("Sikeres regisztráció!");
        }
        response.sendRedirect(getServletName());
    }

}
