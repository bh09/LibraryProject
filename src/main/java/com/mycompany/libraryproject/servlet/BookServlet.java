package com.mycompany.libraryproject.servlet;

import com.mycompany.libraryproject.dto.BookDTO;
import com.mycompany.libraryproject.service.BookService;
import com.mycompany.libraryproject.service.CategoryService;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@ServletSecurity(@HttpConstraint(rolesAllowed = {"admin", "user", "librarian"}))
@WebServlet(name = "BookServlet", urlPatterns = {"/BookServlet"})
public class BookServlet extends HttpServlet {

    @Inject
    BookService bookService;

    @Inject
    CategoryService categoryService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        request.setAttribute("categoryList", categoryService.getAll());
        request.getRequestDispatcher("WEB-INF/book.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        BookDTO bookDTO = new BookDTO();

        bookDTO.setAuthorName(request.getParameter("author"));
        bookDTO.setTitle(request.getParameter("title"));
        bookDTO.setPageNumber(Integer.parseInt(request.getParameter("page")));
        bookDTO.setISBN(request.getParameter("isbn"));
        bookDTO.setCategoryId(Long.parseLong(request.getParameter("category")));

        bookService.save(bookDTO);

        response.sendRedirect(getServletName());
    }

}
