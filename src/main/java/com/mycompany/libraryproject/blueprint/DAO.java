package com.mycompany.libraryproject.blueprint;

import java.util.List;

public interface DAO <E> {

    List<E> getAll();

    Boolean save(E e);

    Boolean delete(E e);

    E get(Long id);

    Boolean update(Long id, E e);

}
