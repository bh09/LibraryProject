package com.mycompany.libraryproject.blueprint;

import java.util.List;

public interface Service <DTO> {

    List<DTO> getAll();
    Boolean save(DTO dto);
    Boolean delete(DTO dto);
    DTO get(Long id);
    Boolean update(DTO dto);

}
