package com.mycompany.libraryproject.service;

import com.mycompany.libraryproject.blueprint.Service;
import com.mycompany.libraryproject.dao.GroupDAO;
import com.mycompany.libraryproject.dao.RoleDAO;
import com.mycompany.libraryproject.dao.UserDAO;
import com.mycompany.libraryproject.dto.RoleDTO;
import com.mycompany.libraryproject.dto.UserDTO;
import com.mycompany.libraryproject.model.Group;
import com.mycompany.libraryproject.model.Role;
import com.mycompany.libraryproject.model.User;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.inject.Singleton;
import lombok.extern.slf4j.Slf4j;
import javax.validation.ConstraintViolationException;
import org.apache.commons.beanutils.BeanUtils;

@Slf4j
@Singleton
public class UserService implements Service<UserDTO> {

    @Inject
    UserDAO userDAO;

    @Inject
    RoleDAO roleDAO;

    @Inject
    GroupDAO groupDAO;

    @Override
    public List<UserDTO> getAll() {
        return userDAO.getAll().stream()
                .map((User u)
                        -> {
                    UserDTO dto = null;
                    try {
                        dto = new UserDTO();
                        dto.setRoleName(u.getRole().getName());
                        BeanUtils.copyProperties(dto, u);
                    } catch (IllegalAccessException | InvocationTargetException ex) {
                        log.error("Getting all users error.");
                    }
                    return dto;
                })
                .collect(Collectors.toList());
    }

    @Override
    public Boolean save(UserDTO dto) {
        try {
            User u = new User();
            Group g = new Group();
            Role role = roleDAO.get(3L);
            
            BeanUtils.copyProperties(u, dto);
            
            g.setEmail(dto.getEmail());
            g.setRoleName("user");
            
            u.setRole(role);

            log.info("User saved. E-mail: " + dto.getEmail());
            return userDAO.save(u) && groupDAO.save(g);
        } catch (IllegalAccessException | InvocationTargetException ex) {
            log.error("User save error. E-mail: " + dto.getEmail());
            return false;
        }
    }

    @Override
    public Boolean delete(UserDTO dto) {
        log.info("User deleted. E-mail: " + dto.getEmail());
        return userDAO.delete(userDAO.get(dto.getId()));
    }

    @Override
    public UserDTO get(Long id) {
        UserDTO dto = null;
        try {
            dto = new UserDTO();
            User user = userDAO.get(id);
            dto.setRoleName(user.getRole().getName());
            BeanUtils.copyProperties(dto, user);
        } catch (IllegalAccessException | InvocationTargetException ex) {
            log.error("Getting user error. Id: " + id.toString());
        }
        return dto;
    }

    @Override
    public Boolean update(UserDTO dto) {
        try {

            User u = new User();
            BeanUtils.copyProperties(u, dto);
            log.info("User updated. E-mail: " + dto.getEmail());
            return userDAO.update(dto.getId(), u);
        } catch (IllegalAccessException | InvocationTargetException ex) {
            log.error("User NOT updated. E-mail: " + dto.getEmail());
            return false;
        }
    }

    public Boolean changeRole(UserDTO userDto, RoleDTO roleDto) {

        try {
            User u = userDAO.get(userDto.getId());
            Role r = roleDAO.get(roleDto.getId());
            Group g = new Group();
            g.setRoleName(r.getName());

            r.setId(roleDto.getId());
            u.setRole(r);

            groupDAO.changeRole(u.getEmail(), g);
            return userDAO.changeRole(userDto.getId(), u);
        } catch (ConstraintViolationException ex) {
            Logger.getLogger(CategoryService.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
}
