package com.mycompany.libraryproject.service;

import com.mycompany.libraryproject.dao.BookDAO;
import com.mycompany.libraryproject.dto.BookDTO;
import com.mycompany.libraryproject.model.Book;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.inject.Singleton;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.BeanUtils;

/**
 *
 * @author wolfp
 */
@Slf4j
@Singleton
public class SearchBookService {

    @Inject
    private BookDAO bookDao;

    public void setBookDao(BookDAO bookDao) {
        this.bookDao = bookDao;
    }

    public List<BookDTO> searchBook(String author, String book, String category) {
        List <Book> books =  bookDao.searchBook(author, book, category);

        log.info("Search: {Author: " + author + ", title: " + book + ", category: " + category + "}");

        return books.stream()
            .map((Book b) ->
                {
                    BookDTO dto = null;
                    try {
                        dto = new BookDTO();
                        dto.setAuthorName(b.getAuthor().getName());
                        dto.setCategoryId(b.getCategory().getId());
                        dto.setCategoryName(b.getCategory().getName());
                        dto.setCover(b.getCover());
                        dto.setSummary(b.getSummary());
                        dto.setId(b.getId());

                        BeanUtils.copyProperties(dto, b);
                    } catch (IllegalAccessException | InvocationTargetException ex) {
                        Logger.getLogger(SearchBookService.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    return dto;
                }
            )
            .collect(Collectors.toList());
    }

}
