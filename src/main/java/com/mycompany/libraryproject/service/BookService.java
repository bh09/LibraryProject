package com.mycompany.libraryproject.service;

import com.mycompany.libraryproject.blueprint.Service;
import com.mycompany.libraryproject.dao.BookDAO;
import com.mycompany.libraryproject.dao.CategoryDAO;
import com.mycompany.libraryproject.dto.BookDTO;
import com.mycompany.libraryproject.model.Author;
import com.mycompany.libraryproject.model.Book;
import com.mycompany.libraryproject.model.Category;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.inject.Singleton;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.BeanUtils;

@Slf4j
@Singleton
public class BookService implements Service<BookDTO> {

    @Inject
    BookDAO bookDao;

    @Inject
    CategoryDAO categoryDAO;

    public void setBookDao(BookDAO bookDao) {
        this.bookDao = bookDao;
    }

    public void setCategoryDAO(CategoryDAO categoryDAO) {
        this.categoryDAO = categoryDAO;
    }

    @Override
    public List<BookDTO> getAll() {
        List<Book> books = bookDao.getAll();

        return books.stream()
            .map((Book b) ->
                {
                    BookDTO dto = null;
                    try {
                        dto = new BookDTO();
                        dto.setAuthorName(b.getAuthor().getName());
                        dto.setCategoryId(b.getCategory().getId());
                        dto.setCategoryName(b.getCategory().getName());
                        BeanUtils.copyProperties(dto, b);
                    } catch (IllegalAccessException | InvocationTargetException ex) {
                        Logger.getLogger(BookService.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    return dto;
                }
            )
            .collect(Collectors.toList());
    }

    @Override
    public Boolean save(BookDTO bookDTO) {
        Author author = new Author();
        author.setName(bookDTO.getAuthorName());

        Category category = categoryDAO.get(bookDTO.getCategoryId());

        Book book = new Book();

        try {
            BeanUtils.copyProperties(book, bookDTO);
            book.setAuthor(author);
            book.setCategory(category);
            log.info("Book saved. " + bookDTO.toString());
            return bookDao.save(book);
        } catch (IllegalAccessException | InvocationTargetException ex) {
            Logger.getLogger(BookService.class.getName()).log(Level.SEVERE, null, ex);
            log.error("Book NOT saved. " + bookDTO.toString());
            return false;
        }
    }

    @Override
    public BookDTO get(Long id) {
        BookDTO dto = null;

        try {
            Book b = bookDao.get(id);
            dto = new BookDTO();
            dto.setAuthorName(b.getAuthor().getName());
            dto.setCategoryId(b.getCategory().getId());
            dto.setCategoryName(b.getCategory().getName());

            BeanUtils.copyProperties(dto, b);
        } catch (IllegalAccessException | InvocationTargetException ex) {
            Logger.getLogger(BookService.class.getName()).log(Level.SEVERE, null, ex);
            log.error("Getting book error. " + id.toString());
        }

        return dto;
    }

    @Override
    public Boolean update(BookDTO bookDTO) {
        Author author = new Author();
        author.setName(bookDTO.getAuthorName());

        Category category = categoryDAO.get(bookDTO.getCategoryId());

        Book book = bookDao.get(bookDTO.getId());

        try {
            book.setAuthor(author);
            book.setCategory(category);
            BeanUtils.copyProperties(book, bookDTO);
            log.info("Book updated. " + bookDTO.toString());
        } catch (IllegalAccessException | InvocationTargetException ex) {
            Logger.getLogger(BookService.class.getName()).log(Level.SEVERE, null, ex);
            log.error("Book NOT updated. " + bookDTO.toString());
            return false;
        }

        return bookDao.update(bookDTO.getId(), book);
    }

    @Override
    public Boolean delete(BookDTO b) {
        Book book = bookDao.get(b.getId());
        if (bookDao.delete(bookDao.get(b.getId()))) {
            log.info("Book deleted: " + b.toString());
            return true;
        }
        log.info("Book NOT deleted: " + b.toString());
        return false;
    }
}
