package com.mycompany.libraryproject.service;

import com.mycompany.libraryproject.blueprint.Service;
import com.mycompany.libraryproject.dao.CategoryDAO;
import com.mycompany.libraryproject.dto.CategoryDTO;
import com.mycompany.libraryproject.model.Category;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.inject.Singleton;
import org.apache.commons.beanutils.BeanUtils;

@Singleton
public class CategoryService implements Service<CategoryDTO> {
    @Inject
    private CategoryDAO categoryDao;

    void setCategoryDao(CategoryDAO categoryDao) {
        this.categoryDao = categoryDao;
    }

    @Override
    public List<CategoryDTO> getAll() {
        List<Category> list = categoryDao.getAll();

        return list.stream()
            .map((Category c) ->
                {
                    CategoryDTO dto = null;
                    try {
                        dto = new CategoryDTO();
                        BeanUtils.copyProperties(dto, c);
                    } catch (IllegalAccessException | InvocationTargetException ex) {
                        Logger.getLogger(CategoryService.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    return dto;
                })
            .collect(Collectors.toList());
    }

    @Override
    public CategoryDTO get(Long id) {
        CategoryDTO dto = null;

        try {
            Category c = categoryDao.get(id);
            dto = new CategoryDTO();
            BeanUtils.copyProperties(dto, c);
        } catch (IllegalAccessException | InvocationTargetException ex) {
            Logger.getLogger(CategoryService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return dto;
    }

    @Override
    public Boolean save(CategoryDTO dto) {
        try {
            Category c = new Category();
            BeanUtils.copyProperties(c, dto);
            return categoryDao.save(c);
        } catch (IllegalAccessException | InvocationTargetException ex) {
            Logger.getLogger(CategoryService.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public Boolean delete(CategoryDTO dto) {
        return categoryDao.delete(categoryDao.get(dto.getId()));
    }

    @Override
    public Boolean update(CategoryDTO dto) {
        try {
            Category c = new Category();
            BeanUtils.copyProperties(c, dto);
            return categoryDao.update(dto.getId(), c);
        } catch (IllegalAccessException | InvocationTargetException ex) {
            Logger.getLogger(CategoryService.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

}
