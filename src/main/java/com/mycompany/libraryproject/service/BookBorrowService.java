package com.mycompany.libraryproject.service;

import com.mycompany.libraryproject.blueprint.Service;
import com.mycompany.libraryproject.dao.BookBorrowDAO;
import com.mycompany.libraryproject.dao.BookDAO;
import com.mycompany.libraryproject.dao.UserDAO;
import com.mycompany.libraryproject.dto.BookStatusDTO;
import com.mycompany.libraryproject.model.Book;
import com.mycompany.libraryproject.model.BookStatus;
import com.mycompany.libraryproject.model.User;
import java.lang.reflect.InvocationTargetException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.inject.Singleton;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.BeanUtils;

@Slf4j
@Singleton
public class BookBorrowService implements Service<BookStatusDTO> {

    @Inject
    BookBorrowDAO bookBorrowingDAO;

    @Inject
    BookDAO bookDAO;

    @Inject
    UserDAO userDAO;

    private BookStatusDTO convert(BookStatus bs) {
        BookStatusDTO bsdto = null;

        try {
            bsdto = new BookStatusDTO();
            BeanUtils.copyProperties(bsdto, bs);

            bsdto.setAuthorName(bs.getBook().getAuthor().getName());

            bsdto.setBookId(bs.getBook().getId());
            bsdto.setBookTitle(bs.getBook().getTitle());

            bsdto.setBookCategory(bs.getBook().getCategory().getName());

            bsdto.setUserId(bs.getUser().getId());
            bsdto.setUserName(bs.getUser().getFirstName() + " " + bs.getUser().getLastName());
        }
        catch (IllegalAccessException | InvocationTargetException ex) {
            Logger.getLogger(BookBorrowService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return bsdto;
    }

    @Override
    public List<BookStatusDTO> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Boolean save(BookStatusDTO dto) {
        try {
            BookStatus bookStatus = new BookStatus();
            BeanUtils.copyProperties(bookStatus, dto);

            bookStatus.setBook(bookDAO.get(dto.getBookId()));
            bookStatus.setUser(userDAO.get(dto.getUserId()));
            bookStatus.setNumberOfExtensions(1L);
            bookStatus.setDateOfExpiration(Timestamp.valueOf(LocalDate.now().plusWeeks(2).atStartOfDay()));

            log.info("Book status saved. " + dto.toString());

            bookBorrowingDAO.save(bookStatus);
        } catch (IllegalAccessException | InvocationTargetException ex) {
            Logger.getLogger(BookBorrowService.class.getName()).log(Level.SEVERE, null, ex);
            log.error("Book status NOT saved. " + dto.toString());
            return false;
        }
        return true;
    }

    @Override
    public Boolean delete(BookStatusDTO dto) {
        log.info("Book deleted. " + dto.toString());
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public BookStatusDTO get(Long id) {
        BookStatus bs = bookBorrowingDAO.get(id);
        return convert(bs);
    }

    @Override
    public Boolean update(BookStatusDTO dto) {
        try {
            Book book = bookDAO.get(dto.getBookId());
            User user = userDAO.get(dto.getUserId());
            BookStatus bookStatus = new BookStatus();
            bookStatus.setBook(book);
            bookStatus.setUser(user);

            BeanUtils.copyProperties(bookStatus, dto);

            bookStatus.setNumberOfExtensions(dto.getNumberOfExtensions());
            bookStatus.setDateOfExpiration(dto.getDateOfExpiration());
            
            log.info("Book status updated. " + dto.toString());

            return bookBorrowingDAO.update(dto.getId(), bookStatus);
        } catch (IllegalAccessException | InvocationTargetException ex) {
            log.error("Book status NOT updated. " + dto.toString());
            return false;
        }
    }

    public List<BookStatusDTO> searchBorrowedBook(BookStatusDTO statusDto, String user) {
        List <BookStatus> bookStatuses = bookBorrowingDAO.searchBorrowedBooksByUserId(statusDto.getUserId());

        return bookStatuses.stream()
           .map((BookStatus bs) -> convert(bs))
           .collect(Collectors.toList());
    }

    public Boolean giveBack(BookStatusDTO dto) {
        try {
            BeanUtils.copyProperties(dto, get(dto.getId()));
        } catch (IllegalAccessException | InvocationTargetException ex) {
            log.error("Book give back error. " + dto.toString());
        }
        dto.setDateOfReturn(Timestamp.valueOf(LocalDate.now().atStartOfDay()));
        log.info("Book gived back. " + dto.toString());
        return update(dto);
    }

    public Boolean borrowExtension(BookStatusDTO dto) {
        Boolean isSuccessful;
        dto = get(dto.getId());
        dto.setNumberOfExtensions(dto.getNumberOfExtensions() + 1);
        dto.setDateOfExpiration(Timestamp.valueOf(dto.getDateOfExpiration().toLocalDateTime().plusWeeks(2)));
        if (isSuccessful = update(dto)) {
            log.info("Borrow extension has successful");
        } else {
            log.error("Borrow extension has failure");
        }
        return isSuccessful;
    }
}
