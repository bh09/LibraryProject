/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.libraryproject.service;

import com.mycompany.libraryproject.blueprint.Service;
import com.mycompany.libraryproject.dao.RoleDAO;
import com.mycompany.libraryproject.dto.RoleDTO;
import com.mycompany.libraryproject.model.Role;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.inject.Singleton;
import org.apache.commons.beanutils.BeanUtils;

/**
 *
 * @author Fanni
 */
@Singleton
public class RoleService implements Service<RoleDTO>{

    @Inject
    RoleDAO roleDAO;

    @Override
    public List<RoleDTO> getAll() {
        return roleDAO.getAll().stream()
                .map((Role r)
                        -> {
                    RoleDTO dto = null;
                    try {
                        dto = new RoleDTO();
                        dto.setName(r.getName());
                        BeanUtils.copyProperties(dto, r);
                    } catch (IllegalAccessException | InvocationTargetException ex) {
                        Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    return dto;
                })
                .collect(Collectors.toList());
    }

    @Override
    public Boolean save(RoleDTO dto) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Boolean delete(RoleDTO dto) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public RoleDTO get(Long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Boolean update(RoleDTO dto) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
