package com.mycompany.libraryproject.dto;

import java.io.Serializable;
import java.sql.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
public class BookDTO implements Serializable {

   @NonNull private Long id;
   @NonNull private String authorName;
   @NonNull private Long categoryId;
   @NonNull private String categoryName;
   @NonNull private String title;
   @NonNull private Integer pageNumber;
   @NonNull private String ISBN;
   @NonNull private Date createdAt;
   private String cover;
   private String summary;

}
