package com.mycompany.libraryproject.dto;

import java.io.Serializable;
import lombok.Data;

/**
 *
 * @author wolfp
 */
@Data
public class AuthorDTO implements Serializable {
    Long id;
    String name;
}
