package com.mycompany.libraryproject.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO {

    Long id;
    String firstName;
    String lastName;
    String roleName;
    String email;
    String password;

}
