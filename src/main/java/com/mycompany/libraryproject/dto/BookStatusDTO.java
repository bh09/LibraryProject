package com.mycompany.libraryproject.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BookStatusDTO implements Serializable {

    Long id;

    String authorName;

    Long bookId;
    String bookTitle;

    String bookCategory;

    Long userId;
    String userName;

    Timestamp dateOfBorrowing;

    Timestamp dateOfExpiration;

    Timestamp dateOfReturn;

    Long numberOfExtensions;

}
