package com.mycompany.libraryproject.model;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "bookstatus")
public class BookStatus implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @JoinColumn(name = "book_id")
    @OneToOne
    private Book book;

    @JoinColumn(name = "user_id")
    @OneToOne
    private User user;

    @Column(name = "dateOfBorrowing")
    @CreationTimestamp
    private Timestamp dateOfBorrowing;

    @Column(name = "dateOfExpiration")
    @CreationTimestamp
    private Timestamp dateOfExpiration;

    @Column(name = "dateOfReturn")
    private Timestamp dateOfReturn;

    @Column(name = "numberOfExtensions")
    private Long numberOfExtensions;
}
