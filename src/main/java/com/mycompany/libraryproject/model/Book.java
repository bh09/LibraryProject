package com.mycompany.libraryproject.model;

import java.io.Serializable;
import java.sql.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@Table(name = "book")
public class Book implements Serializable {

    @Id
    @NonNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @NonNull
    @JoinColumn(name = "author_id")
    private Author author;

    @ManyToOne(cascade = CascadeType.REFRESH)
    @NonNull
    @JoinColumn(name = "category_id")
    private Category category;

    @NonNull
    @Column(name = "title")
    private String title;

    @NonNull
    @Column(name = "pageNumber")
    private Integer pageNumber;

    @NonNull
    @Column(name = "ISBN")
    private String ISBN;

    @NonNull
    @CreationTimestamp
    @Column(name = "createdAt")
    private Date createdAt;


    @Column(name="cover", length = 500)
    private String cover;


    @Column(name="summary", length = 2500)
    private String summary;

}
