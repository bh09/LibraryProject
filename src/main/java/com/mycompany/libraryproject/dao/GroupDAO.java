/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.libraryproject.dao;

import com.mycompany.libraryproject.blueprint.DAO;
import com.mycompany.libraryproject.model.Group;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

/**
 *
 * @author Fanni
 */
@Stateless
@LocalBean
public class GroupDAO implements DAO<Group> {

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public List<Group> getAll() {
        return entityManager.createQuery("SELECT u FROM Group u", Group.class).getResultList();
    }

    @Transactional
    @Override
    public Boolean save(Group e) {

        try {
            entityManager.persist(e);
            return true;
        } catch (ConstraintViolationException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public Boolean delete(Group e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Group get(Long id) {
        return Optional.ofNullable(entityManager.find(Group.class, id)).get();
    }

    @Override
    public Boolean update(Long id, Group e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Group getGroupByEmail(String email) {
        return Optional.ofNullable(
                entityManager.createQuery("SELECT g FROM Group g WHERE g.email = :email",
                        Group.class
                )
                        .setParameter("email", email)
                        .getSingleResult())
                .get();
    }

    @Transactional
    public Boolean changeRole(String email, Group g) {
        Group groupToUpdate = getGroupByEmail(email);

        try {
            groupToUpdate.setRoleName(g.getRoleName());
            entityManager.merge(groupToUpdate);
            return true;
        } catch (ConstraintViolationException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

}
