package com.mycompany.libraryproject.dao;

import com.mycompany.libraryproject.blueprint.DAO;
import com.mycompany.libraryproject.model.Book;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.LoggerFactory;

@Stateless
@LocalBean
public class BookDAO implements DAO<Book> {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(BookDAO.class);

    @PersistenceContext
    private EntityManager entityManager;

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public List<Book> getAll() {
        Query query = entityManager.createQuery("SELECT b FROM Book b", Book.class);
        return query.getResultList();
    }

    @Override
    public Boolean save(Book b) {
        try {
            entityManager.persist(b);
            return true;
        } catch (Exception ex) {
            Logger.getLogger(BookDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public Boolean delete(Book b) {
        try {
            entityManager.remove(entityManager.contains(b) ? b : entityManager.merge(b));
        } catch (Exception ex) {
            Logger.getLogger(BookDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    @Override
    public Book get(Long id) {
        return Optional.ofNullable(entityManager.find(Book.class, id)).get();
    }

    @Override
    public Boolean update(Long id, Book book) {
        try {
            Book bookToUpdate = get(id);

            bookToUpdate.getAuthor().setName(book.getAuthor().getName());
            bookToUpdate.getCategory().setName(book.getCategory().getName());

            BeanUtils.copyProperties(bookToUpdate, book);

            entityManager.merge(bookToUpdate);
        } catch (IllegalAccessException | InvocationTargetException ex) {
            Logger.getLogger(BookDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    public List<Book> searchBook(String author, String title, String category) {

        logger.info("Book search running");
        return entityManager.createQuery(
                "SELECT c FROM Book c "
                + "JOIN c.category v "
                + "JOIN c.author u "
                + "WHERE c.title LIKE :title "
                + "AND u.name LIKE :author "
                + "AND v.name LIKE :category")
                .setParameter("title", '%' + title + '%')
                .setParameter("category", "%" + category + "%")
                .setParameter("author", '%' + author + '%')
                .getResultList();
    }
}
