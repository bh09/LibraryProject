package com.mycompany.libraryproject.dao;

import com.mycompany.libraryproject.blueprint.DAO;
import com.mycompany.libraryproject.model.BookStatus;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.beanutils.BeanUtils;

@Stateless
@LocalBean
public class BookBorrowDAO implements DAO<BookStatus> {

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public List<BookStatus> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Boolean save(BookStatus bs) {
        try {
            entityManager.persist(bs);
            return true;
        } catch (Exception ex) {
            Logger.getLogger(BookBorrowDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public Boolean delete(BookStatus bs) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public BookStatus get(Long id) {
        return Optional.ofNullable(entityManager.find(BookStatus.class, id)).get();
    }

    @Override
    public Boolean update(Long id, BookStatus bs) {
        try {
            BookStatus bookStatusToUpdate = get(id);

            BeanUtils.copyProperties(bookStatusToUpdate, bs);

            entityManager.merge(bookStatusToUpdate);
        } catch (IllegalAccessException | InvocationTargetException ex) {
            Logger.getLogger(BookBorrowDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    public List<BookStatus> searchBorrowedBooksByUserId(Long id) {

        return entityManager.createQuery(
            "SELECT bs FROM BookStatus bs WHERE bs.user.id = :user_id AND bs.dateOfReturn IS NULL", BookStatus.class)
            .setParameter("user_id", id)
            .getResultList();
    }

}
