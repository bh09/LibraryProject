package com.mycompany.libraryproject.dao;

import com.mycompany.libraryproject.blueprint.DAO;
import com.mycompany.libraryproject.model.Category;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.apache.commons.beanutils.BeanUtils;

@Stateless
@LocalBean
public class CategoryDAO implements DAO<Category> {

    @PersistenceContext
    private EntityManager entityManager;

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public List<Category> getAll() {
        Query query = entityManager.createQuery("SELECT c FROM Category c", Category.class);
        return query.getResultList();
    }

    @Override
    public Boolean save(Category c) {
        try {
            entityManager.persist(c);
        } catch (Exception ex) {
            Logger.getLogger(CategoryDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    @Override
    public Boolean delete(Category c) {
        try {
            entityManager.remove(c);
        } catch (Exception ex) {
            Logger.getLogger(CategoryDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    @Override
    public Category get(Long id) {
        return Optional.ofNullable(entityManager.find(Category.class, id)).get();
    }

    @Override
    public Boolean update(Long id, Category category) {
        try {
            Category categoryToUpdate = get(id);

            BeanUtils.copyProperties(categoryToUpdate, category);

            entityManager.merge(categoryToUpdate);
        } catch (IllegalAccessException | InvocationTargetException ex) {
            Logger.getLogger(CategoryDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

}
