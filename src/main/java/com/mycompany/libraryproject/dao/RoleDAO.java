package com.mycompany.libraryproject.dao;

import com.mycompany.libraryproject.blueprint.DAO;
import com.mycompany.libraryproject.model.Role;
import java.util.List;
import java.util.Optional;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
@LocalBean
public class RoleDAO implements DAO<Role> {

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public List<Role> getAll() {
       return entityManager.createQuery("SELECT u FROM Role u", Role.class).getResultList();
    }

    @Override
    public Boolean save(Role e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Boolean delete(Role e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Role get(Long id) {
        return Optional.ofNullable(entityManager.find(Role.class, id)).get();
    }

    public Role getByName(String name) {
        return Optional.ofNullable(entityManager.find(Role.class, name)).get();
    }

    @Override
    public Boolean update(Long id, Role e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
