package com.mycompany.libraryproject.dao;

import com.mycompany.libraryproject.blueprint.DAO;
import com.mycompany.libraryproject.model.User;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;
import org.apache.commons.beanutils.BeanUtils;

@Stateless
@LocalBean
public class UserDAO implements DAO<User> {

    @PersistenceContext
    EntityManager entityManager;

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public List<User> getAll() {
        return entityManager.createQuery("SELECT u FROM User u", User.class).getResultList();
    }

    @Override
    public Boolean save(User u) {
        try {
            entityManager.persist(u);
        } catch (Exception ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    @Override
    public Boolean delete(User u) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public User get(Long id) {
        return Optional.ofNullable(entityManager.find(User.class, id)).get();
    }

    @Override
    public Boolean update(Long id, User u) {
        User userToUpdate = get(id);

        try {
            BeanUtils.copyProperties(userToUpdate, u);
            entityManager.merge(userToUpdate);
            return true;
        } catch (IllegalAccessException | InvocationTargetException | ConstraintViolationException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public Boolean changeRole (Long id, User u) {
        User userToUpdate = get(id);
        
        try {
            BeanUtils.copyProperties(userToUpdate, u);
            entityManager.merge(userToUpdate);
            return true;
        } catch (IllegalAccessException | InvocationTargetException | ConstraintViolationException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
}
