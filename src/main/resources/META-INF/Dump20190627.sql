-- MySQL dump 10.13  Distrib 8.0.16, for Win64 (x86_64)
--
-- Host: localhost    Database: library
-- ------------------------------------------------------
-- Server version	8.0.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `author`
--

LOCK TABLES `author` WRITE;
/*!40000 ALTER TABLE `author` DISABLE KEYS */;
INSERT INTO `author` (id, name) VALUES (1,'J. K. Rowling'),(2,'Stephen Hawking'),(3,'Robert Lawson'),(4,'Dan Brown'),(5,'Jules Verne'),(6,'Rejtő Jenő'),(7,'George Orwell');
/*!40000 ALTER TABLE `author` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` (id, name) VALUES (1,'romantikus'),(2,'sport'),(3,'sci-fi'),(4,'utazás'),(5,'orvosi'),(6,'történelmi'),(7,'horror'),(8,'üzleti'),(9,'sütés-főzés'),(10,'gyerek'),(11,'IT'),(12,'kaland');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` (id, name) VALUES (1,'admin'),(2,'librarian'),(3,'user');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (id, email, firstName, lastName, password, role_id) VALUES (1,'admin@example.teszt','Admin','Admin','5f4dcc3b5aa765d61d8327deb882cf99',1),(2,'wolf.peter@example.teszt','Wolf','Péter','5f4dcc3b5aa765d61d8327deb882cf99',3),(3,'barna.gyula@example.teszt','Barna','Gyula','5f4dcc3b5aa765d61d8327deb882cf99',3),(4,'kozma.gizi@example.teszt','Kozma','Gizi','5f4dcc3b5aa765d61d8327deb882cf99',3),(5,'kis.gabriella@example.teszt','Kis','Gabriella','5f4dcc3b5aa765d61d8327deb882cf99',3),(6,'orosz.gyula@example.teszt','Orosz','Gyula','5f4dcc3b5aa765d61d8327deb882cf99',3),(7,'zombori.katalin@example.teszt','Zombori','Katalin','5f4dcc3b5aa765d61d8327deb882cf99',3),(8,'peter.janos@example.teszt','Péter','János','5f4dcc3b5aa765d61d8327deb882cf99',3),(9,'toth.gizi@example.teszt','Tóth','Gizi','5f4dcc3b5aa765d61d8327deb882cf99',3),(10,'cukor.eva@example.teszt','Cukor','Éva','5f4dcc3b5aa765d61d8327deb882cf99',3),(11,'cukor.viktoria@example.teszt','Cukor','Viktória','5f4dcc3b5aa765d61d8327deb882cf99',3),(12,'kovacs.viktoria@example.teszt','Kovács','Viktória','5f4dcc3b5aa765d61d8327deb882cf99',3),(13,'nagy.patrik@example.teszt','Nagy','Patrik','5f4dcc3b5aa765d61d8327deb882cf99',3),(14,'nemeth.patrik@example.teszt','Németh','Patrik','5f4dcc3b5aa765d61d8327deb882cf99',3),(15,'barna.janos@example.teszt','Barna','János','5f4dcc3b5aa765d61d8327deb882cf99',3);

/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `role_group`
--

LOCK TABLES `role_group` WRITE;
/*!40000 ALTER TABLE `role_group` DISABLE KEYS */;
INSERT INTO `role_group` (id, email, role_name) VALUES (1,'admin@example.teszt','admin'),(2,'wolf.peter@example.teszt','user'),(3,'barna.gyula@example.teszt','user'),(4,'kozma.gizi@example.teszt','user'),(5,'kis.gabriella@example.teszt','user'),(6,'orosz.gyula@example.teszt','user'),(7,'zombori.katalin@example.teszt','user'),(8,'peter.janos@example.teszt','user'),(9,'toth.gizi@example.teszt','user'),(10,'cukor.eva@example.teszt','user'),(11,'cukor.viktoria@example.teszt','user'),(12,'kovacs.viktoria@example.teszt','user'),(13,'nagy.patrik@example.teszt','user'),(14,'nemeth.patrik@example.teszt','user'),(15,'barna.janos@example.teszt','user');

/*!40000 ALTER TABLE `role_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `book`
--

LOCK TABLES `book` WRITE;
/*!40000 ALTER TABLE `book` DISABLE KEYS */;
INSERT INTO `book` (id, author_id, category_id, title, pageNumber, ISBN, createdAt, cover, summary) VALUES (1,1,3,'Harry Potter és a bölcsek köve',286,'978-963-324-453-1','2019-06-27 00:00:00', 'https://kbimages1-a.akamaihd.net/8a1b936f-991c-4012-9ed7-5f1d93dab9e9/1200/1200/False/harry-potter-es-a-bolcsek-kove.jpg', 'A Roxfort Boszorkány- és Varázslóképző Szakiskolában töltött első tanév kemény erőpróba a diákok számára. Harry Potternek nem csupán a vizsgákon kell megfelelnie, de egy életre-halálra szóló küzdelemnek is részese lesz. A tizenegy éves varázslójelölt története...'),(2,1,3,'Harry Potter és a tűz serlege',623,'978-963-324-458-6','2019-06-27 00:00:00','https://pmpub-catalogue.s3-eu-west-1.amazonaws.com/covers/web/9781781103869.jpg','Melyik nemzeti válogatott nyeri a Kviddics Világkupát? Ki lesz a Trirnágus Tusa győztese? Utóbbiért a világ három boszorkány- és varázslóképző tanintézetének legrátermettebb diákjai küzdenek. A világraszóló versengés házigazdája Roxfort, az az iskola, ahová Harry Potter immár negyedévesként érkezik. S ahogy az a felsőbb osztályosoknál már egyáltalán nem különös, Harry és barátai a másik nemet is felfedezik... Ám nem csupán e kellemes izgalmakat ígérő események várnak Harryre és barátaira. Voldemort, a fekete mágusok vezére újból készülődik...Tele van a történet váratlanokkal, véletlenekkel, s miért tagadnánk: rémekkel, szörnyekkel, kísértetekkel. Valahogy annyira tele, mint az életünk.'),(3,4,7,'A Da-Vinci kód',648,'978-963-689-345-3','2019-06-27 00:00:00','https://moly.hu/system/covers/big/covers_61416.jpg?1395363053','A csavaros kód Leonardo műveiben rejtőzik. Kétségbeesett hajsza Európa székesegyházain és kastélyain keresztül. Végül fény derül az évszázadokon át titokban tartott, megdöbbentő igazságra. "Egy lángelme szüleménye." Nelson DeMille A tanulmányi úton Párizsban tartózkodó Robert Langdon szimbólumkutatót telefonon riasztják egy késő esti órán. A Louvre idős kurátorát meggyilkolták a múzeumban, és érthetetlen kódot találtak a holttest mellett. Miközben Langdon és Sophie Veveu, a tehetséges francia titkosírásszakértő a rejtvény megoldásán dolgozik, elképedten fedezi fel a rejtett utalások nyomát Leonardo da Vinci műveiben - noha ezeket az utalásokat mindenki láthatja, a festő zseniálisan álcázta őket. A hagyományos thrillerregények kliséiből kilépő A Da Vinci-kód egyszerre villámgyors, intelligens és többrétegű; részletek sokasága és alapos kutatások eredménye. Az első oldalaktól a váratlan és elképesztő végkifejletig Dan Brown bestsellerszerző már megint mesterien bonyolítja a történetet.'),(4,5,3,'Utazás a Föld középpontja felé',252,'978-963-245-179-4','2019-06-27 00:00:00','https://s03.static.libri.hu/cover/c4/2/741727_5.jpg','Otto Lienbrock, a bogaras hamburgi geológusprofesszor egy titkosírással készült középkori feljegyzésből megtudja, hogy az izlandi Snaefells vulkán kráterén keresztül út nyílik a Föld középpontja felé. Unokaöccse, a jámbor Axel sokkal kevésbé lelkesedik a hajmeresztő vállalkozásért, mégis kénytelen nagybátyjával tartani; az ő elbeszéléséből értesülünk a fantasztikus utazás izgalmas kalandjairól. A jövő nagy álmodója ebben a regényében kevesebbet törődött a tudományos valószínűséggel, mint más műveiben. Maga a föld mélyébe vezető "út" már a regény megírásakor eléggé abszurd ötletnek számított; a föld alatti "másik világ" leírása azonban az író korának földtörténeti ismereteit tükrözi. Verne regénye mégis zavartalan örömet szerezhet az olvasónak, aki a képzelet kalandját, az emberi akaraterő és a tudományos önfeláldozás példáját látja benne.'),(5,5,12,'80 nap alatt a Föld körül',224,'978-963-349-049-5','2019-06-27 00:00:00','https://s01.static.libri.hu/cover/09/5/706416_4.jpg','Verne egyik legsikerültebb és legnépszerűbb regénye, melyet mindenféle műfajban újra és újra feldolgoznak. Pedig már szinte a legapróbb gyerekektől a felnőttekig mindenki tudja, mi történik: Phileas Fogg úr – az előkelő londoni Reform Club tagja – vállalkozik arra, hogy fogadásból 80 nap alatt körülhajózza a Földet. Mégis olyan fordulatos, annyi mulatságos meglepetést tartogat, Verne olyan találóan jellemzi szereplőit, hogy az olvasók újra és újra megjutalmazzák magukat a kötet élményével. Nem csoda, hisz Phileas Fogg és Passepartout fantasztikus kalandokkal teli világkörüli útja soha sem pontosan ugyanúgy zajlik... Meglátod, ha újraolvasod!'),(6,4,12,'Angyalok és démonok',708,'978-963-406-544-9','2019-06-27 00:00:00','https://moly.hu/system/covers/big/covers_485287.jpg','Egy ősrégi, titkos testvériség. Egy pusztító erejű, új fegyver. És egy elképesztő célpont. Robert Langdont, a Harvard világhírű szimbólumkutatóját egy svájci tudományos intézetbe hívják, hogy segítsen megfejteni egy rejtélyes jelet, amelyet egy meggyilkolt fizikus mellkasába égettek. Langdon elképesztő felfedezésre jut: egy több száz éves földalatti szervezet - az Illuminátusok - bosszúhadjáratra készül a katolikus egyház ellen. Langdon mindenáron meg akarja menteni a Vatikánt a hatalmas erejű időzített bombától; a gyönyörű és titokzatos tudóssal, Vittoria Vetrával szövetségben Rómába megy. Hétpecsétes titkokon, veszélyes katakombákon, elhagyatott székesegyházakon és a földkerekség legrejtettebb barlangján át, őrült hajszát indítanak együtt a rég elfeledett Illuminátusok búvóhelye után.'),(7,7,6,'1984',420,'9789634055044','2019-07-14 14:58:00','https://images-na.ssl-images-amazon.com/images/I/514CVwOrybL._SX333_BO1,204,203,200_.jpg','1988-ban kommentálva az 1984-et, a valóságos és jelképes évszám között tűnődően botorkálva szükségszerű a kérdés: a történelmi rémképet illetően érvényes-e, érvényes maradt-e Orwell regénye? Nem és igen. Igen és nem. Nem, ha megkönnyebbülten nyugtázhatjuk, hogy az a totalitárius diktatúra, amely az 1984-ben megjelenik, a regény megírása óta nem valósult meg a valóságos történelemben, és a kommentár fogalmazása közben nincs jele – kopogjuk le, persze – , hogy a közeljövőben bármelyik nagyhatalom megvalósítani kívánná. Igen, ha a tegnapi történelem némely államalakzatára gondolunk. Nem a náci Németországra, nem a sztálini Szovjetunióra, hanem időben közelebbi képződményekre: Enver Hodzsa Albániájára, Pol Pot Kambodzsájára. Hogy jelen idejű államképződményeket a diplomáciai illem okából ne említsünk.');
/*!40000 ALTER TABLE `book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `bookstatus`
--

LOCK TABLES `bookstatus` WRITE;
/*!40000 ALTER TABLE `bookstatus` DISABLE KEYS */;
INSERT INTO bookstatus (id, book_id, user_id, dateOfBorrowing, dateOfExpiration, numberOfExtensions) VALUES (1,3,3,'2019-06-27 00:00:00','2019-07-11 00:00:00',1),(2,6,5,'2019-06-14 00:00:00','2019-06-28 00:00:00',0),(3,1,12,'2019-05-20 00:00:00','2019-06-03 00:00:00',1);
/*!40000 ALTER TABLE `bookstatus` ENABLE KEYS */;
UNLOCK TABLES;

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-27 22:02:44