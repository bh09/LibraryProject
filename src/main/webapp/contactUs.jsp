<%-- 
    Document   : bookPage
    Created on : 2019.07.13., 19:56:44
    Author     : Mark
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="icon" href="img/fav-icon.png" type="image/x-icon" />
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Rólunk</title>

        <!-- Icon css link -->
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Rev slider css -->
        <link href="vendors/revolution/css/settings.css" rel="stylesheet">
        <link href="vendors/revolution/css/layers.css" rel="stylesheet">
        <link href="vendors/revolution/css/navigation.css" rel="stylesheet">

        <!-- Extra plugin css -->
        <link href="vendors/owl-carousel/owl.carousel.min.css" rel="stylesheet">
        <link href="vendors/magnify-popup/magnific-popup.css" rel="stylesheet">


        <link href="css/style.css" rel="stylesheet">
        <link href="css/responsive.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body background="bgimage.png">

        <jsp:include page="/MenuServlet" />

        <section class="banner_area" align = "center">
            <div class="container" align = "center">
                <div class="banner_inner_text" align = "center">
                    <table style="width: 75%" class="table table-striped">
                        <tr>
                            <td align="center"> 
                                Elérhetőségeink </br>
                                Cím: 1134 Budapest, Klapka u. 7.</br>
                                Levelezési cím: 1134 Budapest, Klapka u. 7. </br>
                                Fax: +36/1-666-6666</br>
                                Honlap:<a href="index.jsp"> www.intermouselibrary.hu </a>
                            </td> 
                        </tr>
                    </table>
                </div>
            </div>
        </section>
    </body>

</html>
