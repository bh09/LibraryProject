<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@page import="java.util.List"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <title>Regisztráció</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="icon" href="img/fav-icon.png" type="image/x-icon" />
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Kölcsönzés</title>

        <!-- Icon css link -->
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Rev slider css -->
        <link href="vendors/revolution/css/settings.css" rel="stylesheet">
        <link href="vendors/revolution/css/layers.css" rel="stylesheet">
        <link href="vendors/revolution/css/navigation.css" rel="stylesheet">

        <!-- Extra plugin css -->
        <link href="vendors/owl-carousel/owl.carousel.min.css" rel="stylesheet">
        <link href="vendors/magnify-popup/magnific-popup.css" rel="stylesheet">


        <link href="css/style.css" rel="stylesheet">
        <link href="css/responsive.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script>
            function validate() {
                var form = document.getElementById("registrationForm");
                event.preventDefault();

                var firstName = form.firstName;
                var lastName = form.lastName;
                var email = form.email;
                var password = form.password;
                var conpassword = form.conpassword;

                if (!firstName.value.length) {
                    alert("Vezetéknév nem lehet üres!");
                    return false;
                } else if (!lastName.value.length) {
                    alert("Keresztnév nem lehet üres!");
                    return false;
                } else if (!email.value.length) {
                    alert("Email nem lehet üres!");
                    return false;
                } else if (!password.value.length) {
                    alert("Jelszó nem lehet üres!")
                    return false;
                } else if (password.value != conpassword.value) {
                    alert("A két jelszó nem egyezik!");
                    return false;
                }
                form.submit();
            }
        </script> 
    </head>

    <body background="bgimage.png">

        <jsp:include page="/MenuServlet" />

        <c:if test="${null != alertMsg}">
            <script>
                alert("${alertMsg}");
                window.location = 'LoginServlet';
            </script>
        </c:if>

        <section class="banner_area" align = "center">
            <div class="container" align = "center">
               
                    <form class="contact_us_form row" method="POST" onsubmit="validate()" id="registrationForm" >    
                        <div class="form-group col-lg-12" align = "center">
                            <div class="col-sm-9" width=10px>
                                <input type="text" class="form-control" name="firstName" placeholder="Vezetéknév" width=10px>
                            </div>
                        </div>
                        <div class="form-group col-lg-12" align = "center">

                            <div class="col-sm-9" align = "center">
                                <input type="text" class="form-control" name="lastName" placeholder="Keresztnév" width=10px>
                            </div>
                        </div>
                        <div class="form-group col-lg-12" align = "center">

                            <div class="col-sm-9" align = "center">
                                <input type="text" class="form-control" name="email" placeholder="E-mail" width=10px>
                            </div>
                        </div>
                        <div class="form-group col-lg-12" align = "center">

                            <div class="col-sm-9" align = "center">
                                <input type="password" class="form-control" name="password" placeholder="Jelszó" width=10px>
                            </div>
                        </div>

                        <div class="form-group col-lg-12" align = "center">

                            <div class="col-sm-9" align = "center">
                                <input type="password" class="form-control" name="conpassword" placeholder="Jelszó" width=10px>
                            </div>
                        </div>
                      <div class="col-lg-12" align = "center">

                            <div class="form-check">
                                <input type="checkbox" class="custom-control custom-checkbox" required>
                                <label class="form-check-label" for="defaultCheck1">
                                    Az adatkezelési tájékoztatót elolvastam és elfogadom.
                                </label>
                            </div>  
                        </div> 
                        <div class="form-group col-lg-12" align = "center">
                            <div class="col-sm-offset-3 col-sm-12">
                                <button type="submit" class="more_btn" name="Submit" value="Login">Regisztráció</button>
                            </div>
                        </div>
                    </form>
               
            </div>
        </section>        
    </body>
</html>