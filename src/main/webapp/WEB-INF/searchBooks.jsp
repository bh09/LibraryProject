<%--
    Document   : searchBooks
    Created on : Jul 9, 2019, 3:18:26 PM
    Author     : Fanni
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="icon" href="img/fav-icon.png" type="image/x-icon" />
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Keresés</title>

        <!-- Icon css link -->
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Rev slider css -->
        <link href="vendors/revolution/css/settings.css" rel="stylesheet">
        <link href="vendors/revolution/css/layers.css" rel="stylesheet">
        <link href="vendors/revolution/css/navigation.css" rel="stylesheet">

        <!-- Extra plugin css -->
        <link href="vendors/owl-carousel/owl.carousel.min.css" rel="stylesheet">
        <link href="vendors/magnify-popup/magnific-popup.css" rel="stylesheet">


        <link href="css/style.css" rel="stylesheet">
        <link href="css/responsive.css" rel="stylesheet">

        <script>
            function confirmDelete(author, title) {
                let msg = "Biztos, hogy törlöd a következő\n" + author + " - " + title + " c. könyvet?";
                if (!window.confirm(msg)) {
                    event.preventDefault();
                }
            }
        </script>

        <script>
            function confirmDelete(author, title) {
                let msg = "Biztos, hogy törlöd a következő\n" + author + " - " + title + " c. könyvet?";
                if (!window.confirm(msg)) {
                    event.preventDefault();
                }
            }
        </script>

    </head>
    <body background="bgimage.png">


       <jsp:include page="/MenuServlet" />

        <section class="banner_area" align = "center">
            <div class="container" align = "center">
                <div class="banner_inner_text" align = "center">

                    <form class="contact_us_form row" action="SearchBookServlet" method="POST" >


                        <div class="form-group col-lg-12" align = "center">
                            <div class="col-sm-9" width=10px>
                                <input type="text" class="form-control" name="writer" placeholder="Szerző" width=10px>
                            </div>
                        </div>
                        <div class="form-group col-lg-12" align = "center">

                            <div class="col-sm-9" align = "center">
                                <input type="text" class="form-control" name="bookTitle" placeholder="Könyvcím" >
                            </div>
                        </div>
                        <div class="form-group col-lg-12" align = "center">
                            <div class="col-sm-9" align = "center">
                                <input type="text" class="form-control" name="bookCategory" placeholder="Kategória" >
                            </div>
                        </div>
                        <div class="form-group col-lg-12" align = "center">
                            <div class="col-sm-offset-3 col-sm-12">
                                <button type="submit" class="more_btn">Keresés</button>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </section>

       <c:if test="${resultList.size() > 0}">
           
            <table style="background-color: #ffe0b2; width: 75%" class="table table-striped" align = "center">
            
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Szerző</th>
                        <th scope="col">Cím</th>
                        <th scope="col">Kategória</th>
                        <c:if test="${role == 'admin'}">
                            <th scope="col">Műveletek</th>
                        </c:if>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${resultList}" var="book">
                        <tr>
                            <td>${book.id}</td>
                            <td>${book.authorName}</td>
                            <td>${book.title}</td>
                            <td>${book.categoryName}</td>
                            <td> <a href="/LibraryProject/BookPageServlet?id=${book.id}">Megnézem</a></td>
                            <c:if test="${role == 'admin'}">
                                <td> <a
                                        href="?action=delete&id=${book.id}"
                                        class="btn btn-danger btn-sm"
                                        onclick="confirmDelete('${book.authorName}', '${book.title}')"
                                    > <i class="fa fa-trash"></i> </a> </td>
                            </c:if>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
       </c:if>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="js/jquery-3.2.1.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <!-- Rev slider js -->
        <script src="vendors/revolution/js/jquery.themepunch.tools.min.js"></script>
        <script src="vendors/revolution/js/jquery.themepunch.revolution.min.js"></script>
        <script src="vendors/revolution/js/extensions/revolution.extension.actions.min.js"></script>
        <script src="vendors/revolution/js/extensions/revolution.extension.video.min.js"></script>
        <script src="vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <script src="vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script src="vendors/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
        <script src="vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <!-- Extra plugin css -->
        <script src="vendors/counterup/jquery.waypoints.min.js"></script>

        <script src="vendors/counterup/jquery.counterup.min.js"></script>

        <script src="vendors/counterup/apear.js"></script>
        <script src="vendors/counterup/countto.js"></script>
        <script src="vendors/owl-carousel/owl.carousel.min.js"></script>
        <script src="vendors/magnify-popup/jquery.magnific-popup.min.js"></script>
        <script src="js/smoothscroll.js"></script>
        <script src="vendors/circle-bar/circle-progress.min.js"></script>
        <script src="vendors/circle-bar/plugins.js"></script>
        <script src="vendors/isotope/imagesloaded.pkgd.min.js"></script>
        <script src="vendors/isotope/isotope.pkgd.min.js"></script>

        <!--gmaps Js-->
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script>
        <script src="js/gmaps.min.js"></script>

        <!-- contact js -->
        <script src="js/jquery.form.js"></script>
        <script src="js/jquery.validate.min.js"></script>
        <script src="js/contact.js"></script>

        <script src="js/circle-active.js"></script>
        <script src="js/theme.js"></script>
    </body>
</html>
