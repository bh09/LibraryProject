<%--
    Document   : searchBorrowedBooks
    Created on : Jun 29, 2019, 1:15:02 PM
    Author     : Fanni
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="icon" href="img/fav-icon.png" type="image/x-icon" />
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Kölcsönzött könyvek</title>

        <!-- Icon css link -->
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Rev slider css -->
        <link href="vendors/revolution/css/settings.css" rel="stylesheet">
        <link href="vendors/revolution/css/layers.css" rel="stylesheet">
        <link href="vendors/revolution/css/navigation.css" rel="stylesheet">

        <!-- Extra plugin css -->
        <link href="vendors/owl-carousel/owl.carousel.min.css" rel="stylesheet">
        <link href="vendors/magnify-popup/magnific-popup.css" rel="stylesheet">


        <link href="css/style.css" rel="stylesheet">
        <link href="css/responsive.css" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script src="https://momentjs.com/downloads/moment.js"></script>
        <script>
            function timestampToDate(date) {
                return moment(date).format("YYYY. MM. DD.");
            }
        </script>
    </head>
    <body background="bgimage.png">

        <jsp:include page="/MenuServlet" />

        <c:if test="${userRole == 'admin'}">
            <section class="banner_area" align = "center">
                <div class="container" align = "center">
                    <div class="banner_inner_text" align = "center">
                        <form class="contact_us_form row" action="SearchBorrowedBooksServlet" method="POST">
                            <div class="form-group col-lg-12" align = "center">
                                <div class="col-sm-9" width=10px>
                                    <select class="form-control" name="user">
                                        <c:forEach items="${userList}" var="user">
                                            <option value="${user.id}">${user.firstName} ${user.lastName}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-lg-12" align = "center">
                                <div class="col-sm-offset-3 col-sm-12">
                                    <button type="submit" class="more_btn">Keresés</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </section>

        </c:if>

        <c:if test="${userRole != 'admin'}">
            <section class="banner_area" align = "center">
                <div class="container" align = "center">
                    <div class="banner_inner_text" align = "center"></c:if>
                    </div>
                </div>
            </section>


        <c:if test="${resultList.size() == 0}">
            <section class="banner_area" align = "center">
                <div class="alert alert-info" role="alert">
                    Nincs találat
                </div>
            </section>
        </c:if>

        <c:if test="${resultList.size() > 0}">
            <table style="background-color: #ffe0b2; width: 75%" class="table table-striped" align = "center">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Szerző</th>
                        <th scope="col">Cím</th>
                        <th scope="col">Kategória</th>
                        <th scope="col">Lejárat</th>
                        <th scope="col">Műveletek</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${resultList}" var="bookStatus">
                        <tr>
                            <td> ${bookStatus.id} </td>
                            <td> ${bookStatus.authorName} </td>
                            <td> ${bookStatus.bookTitle} </td>
                            <td> ${bookStatus.bookCategory} </td>
                            <td>
                                <script>
                                    document.write(timestampToDate("${bookStatus.dateOfExpiration}"));
                                </script>
                            </td>
                            <td>
                                <a
                                    href="?action=giveBack&id=${bookStatus.id}"
                                    class="btn btn-success btn-sm"
                                    title="Visszaadás"
                                > <i class="fa fa-check"></i> </a>
                                <a
                                    href="?action=extension&id=${bookStatus.id}"
                                    class="btn btn-info btn-sm"
                                    title="Hosszabbítás 2 héttel"
                                > <i class="fa fa-plus"></i> </a>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </c:if>
    </body>
</html>
