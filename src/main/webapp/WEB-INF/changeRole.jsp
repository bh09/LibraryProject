<%-- 
    Document   : changeRole
    Created on : Jul 13, 2019, 6:13:46 PM
    Author     : Fanni
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="icon" href="img/fav-icon.png" type="image/x-icon" />
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Felhasználói jog</title>

        <!-- Icon css link -->
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Rev slider css -->
        <link href="vendors/revolution/css/settings.css" rel="stylesheet">
        <link href="vendors/revolution/css/layers.css" rel="stylesheet">
        <link href="vendors/revolution/css/navigation.css" rel="stylesheet">

        <!-- Extra plugin css -->
        <link href="vendors/owl-carousel/owl.carousel.min.css" rel="stylesheet">
        <link href="vendors/magnify-popup/magnific-popup.css" rel="stylesheet">


        <link href="css/style.css" rel="stylesheet">
        <link href="css/responsive.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body background="bgimage.png">
        
         <jsp:include page="/MenuServlet" />


        <section class="banner_area" align = "center">
            <div class="container" align = "center">
                <div class="banner_inner_text" align = "center">
                    <form class="contact_us_form row" action="ChangeUserRoleServlet" method="POST" >    
                        <div class="form-group col-lg-12" align = "center">
                            <div class="col-sm-9" width=10px>
                                <select class="form-control" name="user">
                                    <c:forEach items="${userList}" var="user">
                                        <option value="${user.id}">${user.firstName} ${user.lastName}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group col-lg-12" align = "center">

                            <div class="col-sm-9" align = "center">
                                <select class="form-control" name="role">
                                    
                                    <c:forEach items="${roleList}" var="role">
                                        <option value="${role.id}">${role.name}</option>
                                    </c:forEach>
                                   
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-lg-12" align = "center">
                            <div class="col-sm-offset-3 col-sm-12">
                                <button type="submit" class="more_btn">Mentés</button>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </section>     
    </body>
</html>
