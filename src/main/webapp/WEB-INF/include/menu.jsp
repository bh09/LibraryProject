<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<header class="main_menu_area">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span></span>
            <span></span>
            <span></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav">
                <li class="nav-item active"><a class="nav-link" href="index.jsp">Főoldal</a></li>
                <c:if test="${!isLoggedIn}">
                    <li class="nav-item"><a class="nav-link" href="LoginServlet">Belépés</a></li>
                </c:if>
                <c:if test="${!isLoggedIn}">
                    <li class="nav-item"><a class="nav-link" href="Registration">Regisztráció</a></li>
                </c:if>
                <li class="nav-item"><a class="nav-link" href="SearchBookServlet">Keresés</a></li>
                <c:if test="${isLoggedIn}">
                    <li class="nav-item dropdown submenu">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Kölcsönzés
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li class="nav-item"><a class="nav-link" href="SearchBorrowedBooksServlet">Kölcsönzött könyveim</a></li>
                            <li class="nav-item"><a class="nav-link" href="BookBorrowServlet">Új kölcsönzés</a></li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown submenu">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Adatbáziskezelés
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li class="nav-item"><a class="nav-link" href="BookServlet">Könyv hozzáadása</a></li>
                            <li class="nav-item"><a class="nav-link" href="#">Könyv módosítása</a></li>
                        </ul>
                    </li>
                </c:if>
                <li class="nav-item"><a class="nav-link" href="contactUs.jsp">Rólunk</a></li>
                <li class="nav-item"><a class="nav-link" href="#">Kapcsolat</a></li>
                <c:if test="${isLoggedIn}">
                    <li class="nav-item"><a class="nav-link" href="LoginServlet?logout">Kilépés</a></li>
                </c:if>
            </ul>
        </div>
    </nav>
</header>