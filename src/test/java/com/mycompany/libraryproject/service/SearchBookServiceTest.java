package com.mycompany.libraryproject.service;

import com.mycompany.libraryproject.dao.BookDAO;
import com.mycompany.libraryproject.dto.BookDTO;
import com.mycompany.libraryproject.model.Author;
import com.mycompany.libraryproject.model.Book;
import com.mycompany.libraryproject.model.Category;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author Fanni
 */
@ExtendWith(MockitoExtension.class)
public class SearchBookServiceTest {

    @Mock
    private BookDAO bookDAO;

    SearchBookService underTest = new SearchBookService();

    @BeforeEach
    private void setMocks() {
        underTest.setBookDao(bookDAO);
    }

    public List<Book> createBookList() {
        List<Book> books = new ArrayList<>();

        Author authorExample = new Author("Stephen King");
        Category categoryExample = new Category(1L, "Horror");

        books.add(new Book(1L, authorExample, categoryExample, "It", 1174, "123456789", Date.valueOf(LocalDate.now())));
        books.add(new Book(2L, authorExample, categoryExample, "Shining", 1174, "987654321", Date.valueOf(LocalDate.now())));

        return books;

    }

    public List<BookDTO> createBookDtoList() {
        List<BookDTO> books = new ArrayList<>();

        books.add(new BookDTO(1L, "Stephen King", 1L, "Horror", "It", 1174, "123456789", Date.valueOf(LocalDate.now())));

        return books;
    }

    @Test
    public void testSearch() {
        // Given
        List<Book> testList = new ArrayList<>();
        testList.add(createBookList().get(0));
        Mockito.when(bookDAO.searchBook("", "It", "")).thenReturn(testList);

        // When
        List<BookDTO> bookDtos = underTest.searchBook("", "It", "");

        // Then
        assertEquals(createBookDtoList(), bookDtos);
    }

}
