package com.mycompany.libraryproject.service;

import com.mycompany.libraryproject.dao.UserDAO;
import com.mycompany.libraryproject.dto.UserDTO;
import com.mycompany.libraryproject.model.Role;
import com.mycompany.libraryproject.model.User;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    @Mock
    UserDAO userDAO;

    @InjectMocks
    private UserService underTest = new UserService();

    private List<User> createUserList() {
        List<User> users = new ArrayList();
        Role role = new Role(0L, "Admin");
        users.add(new User(0L, role, "admin@examle.com", ",alé,l@a,sdéa", "Szekeres", "Béla"));
        users.add(new User(1L, role, "geza.admin@examle.com", ",alé,l@a,sadl", "Szekeres", "Géza"));
        return users;
    }

    private List<UserDTO> createUserDTOList() {
        List<UserDTO> userDTOs = new ArrayList();
        userDTOs.add(new UserDTO(0L, "Szekeres", "Béla", "Admin", "admin@examle.com", ",alé,l@a,sdéa"));
        userDTOs.add(new UserDTO(1L, "Szekeres", "Géza", "Admin", "geza.admin@examle.com", ",alé,l@a,sadl"));
        return userDTOs;
    }

    @Test
    public void testGetAll() {
        // Given
        Mockito.when(userDAO.getAll()).thenReturn(createUserList());

        // When
        List<UserDTO> userDTOs = underTest.getAll();

        // Then
        assertEquals(createUserDTOList(), userDTOs);
    }

    @Test
    public void testGet() {
        // Given
        Mockito.when(userDAO.get(0L)).thenReturn(createUserList().get(0));

        // When
        UserDTO userDTO = underTest.get(0L);

        // Then
        assertEquals(createUserDTOList().get(0), userDTO);
    }
}
