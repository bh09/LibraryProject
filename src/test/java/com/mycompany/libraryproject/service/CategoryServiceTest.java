package com.mycompany.libraryproject.service;

import com.mycompany.libraryproject.dao.CategoryDAO;
import com.mycompany.libraryproject.dto.CategoryDTO;
import com.mycompany.libraryproject.model.Category;
import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class CategoryServiceTest {

    @Mock
    private CategoryDAO categoryDao;

    private CategoryService underTest = new CategoryService();

    @BeforeEach
    public void setMocks() {
        underTest.setCategoryDao(categoryDao);
    }

    private List<Category> createCategoryList() {
        List<Category> categories = new ArrayList<>();
        Category c = new Category();

        c.setId(0L);
        c.setName("Mistery");

        categories.add(c);

        return categories;
    }

    private List<CategoryDTO> createCategoryDTOList() {
        List<CategoryDTO> categoryDTOs = new ArrayList<>();
        CategoryDTO cDto = new CategoryDTO();

        cDto.setId(0L);
        cDto.setName("Mistery");

        categoryDTOs.add(cDto);

        return categoryDTOs;
    }

    @Test
    public void testGetAll() {
        // Given
        Mockito.when(categoryDao.getAll()).thenReturn(createCategoryList());

        // When
        List<CategoryDTO> result = underTest.getAll();

        // Then
        assertEquals(createCategoryDTOList(), result);
    }

    @Test
    public void testGet() {
        // Given
        Category c = createCategoryList().get(0);
        Mockito.when(categoryDao.get(0L)).thenReturn(c);

        // When
        CategoryDTO result = underTest.get(0L);

        // Then
        assertEquals(createCategoryDTOList().get(0), result);
    }

    @Test
    public void testSave() {
        // Given
        Category c = createCategoryList().get(0);
        Mockito.when(categoryDao.save(c)).thenReturn(true);

        // When
        Boolean result = underTest.save(createCategoryDTOList().get(0));

        // Then
        assertTrue(result);
    }

    @Test
    public void testUpdate() {
        // Given
        Category c = createCategoryList().get(0);
        Category c2 = new Category(1L, "Fantasy");
        Mockito.when(categoryDao.save(c)).thenReturn(true);
        Mockito.when(categoryDao.update(1L, c2)).thenReturn(true);

        // When
        categoryDao.save(c);
        CategoryDTO categoryDTO = new CategoryDTO(1L, "Fantasy");

        Boolean result = underTest.update(categoryDTO);

        // Then
        assertTrue(result);
    }

    @Test
    public void testDelete() {
        // Given
        Category c = createCategoryList().get(0);
        CategoryDTO cDto = createCategoryDTOList().get(0);

        Mockito.when(categoryDao.get(cDto.getId())).thenReturn(c);
        Mockito.when(categoryDao.delete(c)).thenReturn(true);

        // When
        Boolean result = underTest.delete(createCategoryDTOList().get(0));

        // Then
        assertTrue(result);
    }
}
