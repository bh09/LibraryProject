package com.mycompany.libraryproject.service;

import com.mycompany.libraryproject.dao.BookDAO;
import com.mycompany.libraryproject.dao.CategoryDAO;
import com.mycompany.libraryproject.dto.BookDTO;
import com.mycompany.libraryproject.model.Author;
import com.mycompany.libraryproject.model.Book;
import com.mycompany.libraryproject.model.Category;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class BookServiceTest {

    @Mock
    private CategoryDAO categoryDAO;

    @Mock
    private BookDAO bookDAO;

    BookService underTest = new BookService();

    @BeforeEach
    private void setMocks() {
        underTest.setBookDao(bookDAO);
        underTest.setCategoryDAO(categoryDAO);
    }

    private List<Book> createBookList() {
        List<Book> books = new ArrayList<>();
        Author author = new Author("Franz Kafka");
        Category category = new Category(0L, "Szürrealizmus");
        books.add(new Book(0L, author, category, "Átváltozás", 352, "9789634052692", Date.valueOf(LocalDate.now())));

        return books;
    }

    private List<BookDTO> createBookDTOList() {
        List<BookDTO> bookDTOs = new ArrayList<>();
        bookDTOs.add(new BookDTO(0L, "Franz Kafka", 0L, "Szürrealizmus", "Átváltozás", 352, "9789634052692", Date.valueOf(LocalDate.now())));

        return bookDTOs;
    }

    private Book createNewBook() {
        Author author = new Author("Kafka Franz");
        Category category = new Category(1L, "Abszurd");

        return new Book(0L, author, category, "Átváltozás", 352, "9789634052692", Date.valueOf(LocalDate.now()));
    }

    private BookDTO createNewBookDTO() {
        return new BookDTO(0L, "Kafka Franz", 1L, "Abszurd", "Átváltozás", 352, "9789634052692", Date.valueOf(LocalDate.now()));
    }

    @Test
    public void testGetAll() {
        // Given
        Mockito.when(bookDAO.getAll()).thenReturn(createBookList());

        // When
        List<BookDTO> bookDTOs = underTest.getAll();

        // Then
        assertEquals(createBookDTOList(), bookDTOs);
    }

    @Test
    public void testGet() {
        // Given
        Mockito.when(bookDAO.get(0L)).thenReturn(createBookList().get(0));

        // When
        BookDTO bookDTO = underTest.get(0L);

        // Then
        assertEquals(createBookDTOList().get(0), bookDTO);
    }

    @Test
    public void testSave() {
        // Given
        Mockito.when(categoryDAO.get(0L)).thenReturn(createBookList().get(0).getCategory());
        Mockito.when(bookDAO.save(createBookList().get(0))).thenReturn(true);

        // When
        Boolean result = underTest.save(createBookDTOList().get(0));

        // Then
        assertTrue(result);
    }

    @Test
    public void testUpdate() {
        // Given
        Mockito.when(bookDAO.get(0L)).thenReturn(createNewBook());
        Mockito.when(categoryDAO.get(1L)).thenReturn(createNewBook().getCategory());
        Mockito.when(bookDAO.update(0L, createNewBook())).thenReturn(true);

        // When
        Boolean result = underTest.update(createNewBookDTO());

        // Then
        assertTrue(result);
        assertEquals(createNewBookDTO(), underTest.get(0L));
    }

    @Test
    public void testDelete() {
        // Given
        Mockito.when(bookDAO.get(0L)).thenReturn(createBookList().get(0));
        Mockito.when(bookDAO.delete(createBookList().get(0))).thenReturn(true);

        Mockito.when(bookDAO.getAll()).thenReturn(new ArrayList<>());

        // When
        Boolean result = underTest.delete(createBookDTOList().get(0));

        // Then
        assertTrue(result);
        assertEquals(0, underTest.getAll().size());
    }

}
