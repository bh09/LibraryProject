package com.mycompany.libraryproject.service;

import com.mycompany.libraryproject.dao.BookBorrowDAO;
import com.mycompany.libraryproject.dao.BookDAO;
import com.mycompany.libraryproject.dao.UserDAO;
import com.mycompany.libraryproject.dto.BookStatusDTO;
import com.mycompany.libraryproject.model.Author;
import com.mycompany.libraryproject.model.Book;
import com.mycompany.libraryproject.model.BookStatus;
import com.mycompany.libraryproject.model.Category;
import com.mycompany.libraryproject.model.Role;
import com.mycompany.libraryproject.model.User;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class BookBorrowingServiceTest {

    @Mock
    BookBorrowDAO bookBorrowDAO;

    @Mock
    BookDAO bookDAO;

    @Mock
    UserDAO userDAO;

    @InjectMocks
    private BookBorrowService underTest;

    private BookStatus createBookStatus() {
        Author author = new Author("Franz Kafka");
        Category category = new Category(0L, "Szürrealizmus");
        Book book = new Book(0L, author, category, "Átváltozás", 320, "9789634052692", Date.valueOf(LocalDate.now()));
        Role role = new Role(0L, "Admin");
        User user = new User(0L, role, "admin@example.com", "a,ls,dlé,32lé", "Szekeres", "Béla");

        return new BookStatus(
            0L,
            book,
            user,
            Timestamp.valueOf(LocalDate.now().atStartOfDay()),
            Timestamp.valueOf(LocalDate.now().plusWeeks(2).atStartOfDay()),
            null,
            1L
        );
    }

    private BookStatusDTO createBookStatusDTO() {
        Author author = new Author("Franz Kafka");
        Category category = new Category(0L, "Szürrealizmus");
        Book book = new Book(0L, author, category, "Átváltozás", 320, "9789634052692", Date.valueOf(LocalDate.now()));
        Role role = new Role(0L, "Admin");
        User user = new User(0L, role, "admin@example.com", "a,ls,dlé,32lé", "Szekeres", "Béla");

        return new BookStatusDTO(
            0L,
            author.getName(),
            book.getId(),
            book.getTitle(),
            category.getName(),
            user.getId(),
            user.getFirstName() + " " + user.getLastName(),
            Timestamp.valueOf(LocalDate.now().atStartOfDay()),
            Timestamp.valueOf(LocalDate.now().plusWeeks(2).atStartOfDay()),
            null,
            1L
        );
    }

    @Test
    public void testGet() {
        // Given
        Mockito.when(bookBorrowDAO.get(0L)).thenReturn(createBookStatus());

        // When
        BookStatusDTO result = underTest.get(0L);

        // Then
        assertEquals(createBookStatusDTO(), result);
    }

    @Test
    public void testSave() {
        // Given
        Mockito.when(userDAO.get(createBookStatus().getUser().getId())).thenReturn(createBookStatus().getUser());
        Mockito.when(bookDAO.get(createBookStatus().getBook().getId())).thenReturn(createBookStatus().getBook());
        Mockito.when(bookBorrowDAO.save(createBookStatus())).thenReturn(true);

        // When
        Boolean result = underTest.save(createBookStatusDTO());

        // Then
        assertTrue(result);
    }

    @Test
    public void testUpdate() {
        // Given
        Mockito.when(bookDAO.get(0L)).thenReturn(createBookStatus().getBook());
        Mockito.when(userDAO.get(0L)).thenReturn(createBookStatus().getUser());
        Mockito.when(bookBorrowDAO.get(0L)).thenReturn(createBookStatus());
        Mockito.when(bookBorrowDAO.update(0L, createBookStatus())).thenReturn(true);

        // When
        Boolean result = underTest.update(createBookStatusDTO());

        // Then
        assertTrue(result);
        assertEquals(createBookStatusDTO(), underTest.get(0L));
    }

    @Test
    public void testGiveBack() {
        // Given
        BookStatus returnedBookStatus = createBookStatus();
        returnedBookStatus.setDateOfReturn(Timestamp.valueOf(LocalDate.now().atStartOfDay()));

        Mockito.when(bookDAO.get(0L)).thenReturn(createBookStatus().getBook());
        Mockito.when(userDAO.get(0L)).thenReturn(createBookStatus().getUser());
        Mockito.when(bookBorrowDAO.get(0L)).thenReturn(returnedBookStatus);
        Mockito.when(bookBorrowDAO.update(0L, returnedBookStatus)).thenReturn(true);

        // When
        Boolean result = underTest.giveBack(createBookStatusDTO());
        BookStatusDTO dto = underTest.get(0L);

        // Then
        assertTrue(result);
        assertNotEquals(createBookStatusDTO(), dto);
        assertNotNull(dto.getDateOfReturn());
    }

    @Test
    public void testBorrowExtension() throws IllegalAccessException {
        // Given
        Timestamp ts = Timestamp.valueOf(LocalDate.now().plusWeeks(4).atStartOfDay());

        BookStatus bookStatus = createBookStatus();
        bookStatus.setDateOfExpiration(ts);
        bookStatus.setNumberOfExtensions(2L);

        BookStatusDTO bookStatusDTO = createBookStatusDTO();
        bookStatusDTO.setDateOfExpiration(ts);
        bookStatusDTO.setNumberOfExtensions(2L);

        Mockito.when(bookDAO.get(0L)).thenReturn(createBookStatus().getBook());
        Mockito.when(userDAO.get(0L)).thenReturn(createBookStatus().getUser());
        Mockito.when(bookBorrowDAO.get(0L)).thenReturn(createBookStatus(), bookStatus);

        Mockito.when(bookBorrowDAO.update(0L, bookStatus)).thenReturn(true);
        Mockito.when(underTest.update(bookStatusDTO)).thenReturn(true);

        // When
        Boolean result = underTest.borrowExtension(createBookStatusDTO());

        // Then
        assertTrue(result);
    }
}
