package com.mycompany.libraryproject.dao;

import com.mycompany.libraryproject.dto.BookStatusDTO;
import com.mycompany.libraryproject.model.Author;
import com.mycompany.libraryproject.model.Book;
import com.mycompany.libraryproject.model.BookStatus;
import com.mycompany.libraryproject.model.Category;
import com.mycompany.libraryproject.model.Role;
import com.mycompany.libraryproject.model.User;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import org.junit.jupiter.api.Assertions;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class BookBorrowDAOTest {

    @Mock
    EntityManager entityManager;

    @InjectMocks
    BookBorrowDAO underTest = new BookBorrowDAO();

    private BookStatus createBookStatus() {
        Author author = new Author("Franz Kafka");
        Category category = new Category(0L, "Szürrealizmus");
        Book book = new Book(0L, author, category, "Átváltozás", 320, "9789634052692", Date.valueOf(LocalDate.now()));
        Role role = new Role(0L, "Admin");
        User user = new User(0L, role, "admin@example.com", "a,ls,dlé,32lé", "Szekeres", "Béla");

        BookStatus bs = new BookStatus(0L, book, user, Timestamp.valueOf(LocalDate.now().atStartOfDay()), Timestamp.valueOf(LocalDate.now().plusWeeks(2).atStartOfDay()), null, 1L);
        return bs;
    }

    private BookStatus createNewBookStatus() {
        Author author = new Author("Franz Kafka");
        Category category = new Category(0L, "Abszurd");
        Book book = new Book(0L, author, category, "Átváltozás", 320, "9789634052692", Date.valueOf(LocalDate.now()));
        Role role = new Role(0L, "Admin");
        User user = new User(1L, role, "pista@example.com", "a,ls,dlé,32lé", "Szekeres", "Pista");

        BookStatus bs = new BookStatus(0L, book, user, Timestamp.valueOf(LocalDate.now().atStartOfDay()), Timestamp.valueOf(LocalDate.now().plusWeeks(2).atStartOfDay()), null, 1L);
        return bs;
    }

    private List<BookStatus> createBookStatusList() {
        List<BookStatus> bookStatuses = new ArrayList<>();

        bookStatuses.add(createBookStatus());
        bookStatuses.add(createNewBookStatus());

        return bookStatuses;
    }

    private BookStatusDTO createBookStatusDTO() {
        Author author = new Author("Franz Kafka");
        Category category = new Category(0L, "Szürrealizmus");
        Book book = new Book(0L, author, category, "Átváltozás", 320, "9789634052692", Date.valueOf(LocalDate.now()));
        Role role = new Role(0L, "Admin");
        User user = new User(0L, role, "admin@example.com", "a,ls,dlé,32lé", "Szekeres", "Béla");

        BookStatusDTO bookStatusDTO = new BookStatusDTO(
                0L,
                author.getName(),
                book.getId(),
                book.getTitle(),
                category.getName(),
                user.getId(),
                user.getFirstName() + " " + user.getLastName(),
                Timestamp.valueOf(LocalDate.now().atStartOfDay()),
                Timestamp.valueOf(LocalDate.now().plusWeeks(2).atStartOfDay()),
                null,
                1L);
        return bookStatusDTO;
    }

    @Test
    public void testSave() {
        // Given
        Mockito.doNothing().doThrow(IllegalArgumentException.class)
                .when(entityManager)
                .persist(createBookStatus());

        // When
        Boolean result = underTest.save(createBookStatus());

        // Then
        Assertions.assertTrue(result);
    }

    @Test
    public void testGet() {
        // Given
        Mockito.when(entityManager.find(BookStatus.class, 0L)).thenReturn(createBookStatus());

        // When
        BookStatus bookStatus = underTest.get(0L);

        // Then
        assertEquals(createBookStatus(), bookStatus);
    }

    @Test
    public void testUpdate() {
        // Given
        Mockito.when(entityManager.find(BookStatus.class, 0L)).thenReturn(createBookStatus());
        Mockito.when(underTest.get(0L)).thenReturn(createBookStatus());

        // When
        Boolean result = underTest.update(0L, createNewBookStatus());

        // Then
        assertTrue(result);
        assertEquals(createNewBookStatus(), underTest.get(0L));
        assertNotEquals(createBookStatus(), underTest.get(0L));
    }

    @Test
    public void testSearchBorrowedBooks() {
        // Given
        List<BookStatus> filteredByUserList = new ArrayList<>();
        filteredByUserList.add(createBookStatus());
        TypedQuery<BookStatus> query = Mockito.mock(TypedQuery.class);

        Mockito
            .when(
                entityManager.createQuery(
                    "SELECT bs FROM BookStatus bs WHERE bs.user.id = :user_id AND bs.dateOfReturn IS NULL",
                    BookStatus.class
                )
            )
            .thenReturn(query);
        Mockito.when(query.setParameter("user_id", 0L)).thenReturn(query);
        Mockito.when(query.getResultList()).thenReturn(filteredByUserList);

        // When
        List<BookStatus> resultList = underTest.searchBorrowedBooksByUserId(0L);

        // Then
        assertEquals(resultList, filteredByUserList);
    }

}
