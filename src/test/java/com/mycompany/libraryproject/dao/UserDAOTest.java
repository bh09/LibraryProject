package com.mycompany.libraryproject.dao;

import com.mycompany.libraryproject.model.Role;
import com.mycompany.libraryproject.model.User;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class UserDAOTest {

    @Mock
    EntityManager entityManager;

    UserDAO underTest = new UserDAO();

    @BeforeEach
    private void setMocks() {
        underTest.setEntityManager(entityManager);
    }

    private List<User> createUserList() {
        List<User> users = new ArrayList<>();
        Role role = new Role(0L, "Admin");
        users.add(new User(0L, role, "admin@example.com", "alémdmm3émLÉLD?L@swq23", "Szekeres", "Béla"));
        users.add(new User(1L, role, "geza.admin@example.com", "alémdmm3émLÉLD?L@mas", "Szekeres", "Géza"));

        return users;
    }

    @Test
    public void testGetAll() {
        // Given
        TypedQuery<User> query = Mockito.mock(TypedQuery.class);

        Mockito.when(entityManager.createQuery("SELECT u FROM User u", User.class)).thenReturn(query);
        Mockito.when(query.getResultList()).thenReturn(createUserList());

        // When
        List<User> users = underTest.getAll();

        // Then
        assertEquals(createUserList(), users);
    }

    @Test
    public void testGet() {
        // Given
        Mockito.when(entityManager.find(User.class, 0L)).thenReturn(createUserList().get(0));

        // When
        User user = underTest.get(0L);

        // Then
        assertEquals(createUserList().get(0), user);
    }
}
