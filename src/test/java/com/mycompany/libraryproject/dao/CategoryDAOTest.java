
package com.mycompany.libraryproject.dao;

import com.mycompany.libraryproject.model.Category;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class CategoryDAOTest {

    @Mock
    EntityManager entityManager;

    CategoryDAO underTest = new CategoryDAO();

    @BeforeEach
    private void setMocks() {
        underTest.setEntityManager(entityManager);
    }

    private List<Category> createCategoryList() {

        List<Category> categories = new ArrayList<>();

        Category category = new Category(0L, "Fantasy");

        categories.add(0, category);
        return categories;
    }

    private Category createNewCategory() {

        Category category = new Category(0L, "Gyerekkönyv");

        return new Category(0L, "Gyerekkönyv");
    }

    @Test
    public void testGetAll() {
        // Given
        TypedQuery<Category> query = Mockito.mock(TypedQuery.class);

        Mockito.when(entityManager.createQuery("SELECT c FROM Category c", Category.class)).thenReturn(query);
        Mockito.when(query.getResultList()).thenReturn(createCategoryList());

        // When
        List<Category> categories = underTest.getAll();

        // Then
        assertEquals(createCategoryList(), categories);
    }

    @Test
    public void testGet() {
        // Given
        Mockito.when(entityManager.find(Category.class, 0L)).thenReturn(createCategoryList().get(0));

        // When
        Category category = underTest.get(0L);

        // Then
        assertEquals(createCategoryList().get(0), category);
    }

    @Test
    public void testSave() {
        // Given
        Mockito.when(entityManager.find(Category.class, 0L)).thenReturn(createCategoryList().get(0));
        Mockito.when(underTest.get(0L)).thenReturn(createCategoryList().get(0));

        // When
        Boolean result = underTest.save(createCategoryList().get(0));

        // Then
        assertTrue(result);
        assertNotEquals(createNewCategory(), underTest.get(0L));
        assertEquals(createCategoryList().get(0), underTest.get(0L));
    }

    @Test
    public void testDelete() {
        // Given
        Mockito.doNothing().doThrow(IllegalArgumentException.class).when(entityManager).remove(createCategoryList().get(0));

        // When
        Boolean result = underTest.delete(createCategoryList().get(0));

        // Then
        assertTrue(result);
    }

    @Test
    public void testUpdate() {
        // Given
        Mockito.when(entityManager.find(Category.class, 0L)).thenReturn(createCategoryList().get(0));
        Mockito.when(underTest.get(0L)).thenReturn(createCategoryList().get(0));

        // When
        Boolean result = underTest.update(0L, createNewCategory());

        // Then
        assertTrue(result);
        assertEquals(createNewCategory(), underTest.get(0L));
        assertNotEquals(createCategoryList().get(0), underTest.get(0L));
    }

}
