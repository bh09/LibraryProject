package com.mycompany.libraryproject.dao;

import com.mycompany.libraryproject.model.Author;
import com.mycompany.libraryproject.model.Book;
import com.mycompany.libraryproject.model.Category;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class BookDAOTest {

    @Mock
    EntityManager entityManager;

    @InjectMocks
    BookDAO underTest = new BookDAO();

    private List<Book> createBookList() {
        List<Book> books = new ArrayList<>();
        Author author = new Author(0L, "Franz Kafka");
        Category category = new Category(0L, "Szürreális");

        books.add(new Book(0L, author, category, "Átváltozás", 320, "9789634052692", Date.valueOf(LocalDate.now())));
        return books;
    }

    private Book createNewBook() {
        Author author = new Author(0L, "Kafka Franz");
        Category category = new Category(0L, "Abszurd");

        return new Book(0L, author, category, "Átváltozás", 320, "9789634052692", Date.valueOf(LocalDate.now()));
    }

    @Test
    public void testGetAll() {
        // Given
        TypedQuery<Book> query = Mockito.mock(TypedQuery.class);

        Mockito.when(entityManager.createQuery("SELECT b FROM Book b", Book.class)).thenReturn(query);
        Mockito.when(query.getResultList()).thenReturn(createBookList());

        // When
        List<Book> books = underTest.getAll();

        // Then
        assertEquals(createBookList(), books);
    }

    @Test
    public void testGet() {
        // Given
        Mockito.when(entityManager.find(Book.class, 0L)).thenReturn(createBookList().get(0));

        // When
        Book book = underTest.get(0L);

        // Then
        assertEquals(createBookList().get(0), book);
    }

    @Test
    public void testSave() {
        // Given
        Mockito.when(entityManager.find(Book.class, 0L)).thenReturn(createBookList().get(0));
        Mockito.when(underTest.get(0L)).thenReturn(createBookList().get(0));

        // When
        Boolean result = underTest.save(createBookList().get(0));

        // Then
        assertTrue(result);
        assertNotEquals(createNewBook(), underTest.get(0L));
        assertEquals(createBookList().get(0), underTest.get(0L));
    }

    @Test
    public void testUpdate() {
        // Given
        Mockito.when(entityManager.find(Book.class, 0L)).thenReturn(createBookList().get(0));
        Mockito.when(underTest.get(0L)).thenReturn(createBookList().get(0));

        // When
        Boolean result = underTest.update(0L, createNewBook());

        // Then
        assertTrue(result);
        assertEquals(createNewBook(), underTest.get(0L));
        assertNotEquals(createBookList().get(0), underTest.get(0L));
    }

    @Test
    public void testDelete() {
        // Given
        Mockito.doReturn(true).when(entityManager).contains(createBookList().get(0));
        Mockito.doNothing().doThrow(IllegalArgumentException.class).when(entityManager).remove(createBookList().get(0));

        // When
        Boolean result = underTest.delete(createBookList().get(0));

        // Then
        assertTrue(result);
    }

}
